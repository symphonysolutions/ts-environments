# Terraform "Terra-service" with Kubernetes
This demo is based on [this](https://blog.gruntwork.io/a-comprehensive-guide-to-terraform-b3d32832baca) blog post, [this](https://www.youtube.com/watch?v=wgzgVm7Sqlk) talk and my own experience of using Terraform.

## Related repositories:
[Base modules](https://bitbucket.org/gshvet/ts-base-modules)

[Service modules](https://bitbucket.org/gshvet/ts-service-modules)

[Environments](https://bitbucket.org/gshvet/ts-environments)

### How to use it
1. Install minikube.
2. Start minikube cluster.
3. Clone [Environments](https://bitbucket.org/gshvet/ts-environments) repo.
4. Run `terraform init` and `terraform apply` from cloned repo's directory. 
