module "dev1" {
  //NOTE: Reference to a specific branch/tag/revision in a dependency repo
  source           = "bitbucket.org/gshvet/ts-service-modules/?ref=1.0.0"
  env_level        = "dev1"
  app_server_tag   = "8.0-alpine"
  proxy_server_tag = "1.12-alpine"
}

module "dev2" {
  //NOTE: Reference to a specific branch/tag/revision in a dependency repo
  source           = "bitbucket.org/gshvet/ts-service-modules/?ref=1.0.1"
  env_level        = "dev2"
  app_server_tag   = "9.0-alpine"
  proxy_server_tag = "1.13-alpine"
}
